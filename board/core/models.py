import os
import uuid
import hashlib
from django.db import models
from django.utils import timezone
import django.urls
from . import config
from .abstracts import markup


class Banned(BaseException):
    def __init__(self, ban):
        super().__init__()
        self.ban = ban


class WrongPassword(BaseException):
    pass


class ContentMissing(BaseException):
    pass


class Ban(models.Model):
    ip = models.GenericIPAddressField(primary_key=True)
    reason = models.TextField()
    expires_at = models.DateTimeField(db_index=True)

    def __str__(self):
        return f'{self.ip}: {self.reason}'

    @staticmethod
    def create_with_duration(ip, reason, duration):
        return Ban(ip=ip, reason=reason, expires_at=timezone.now() + duration)

    @staticmethod
    def check_ip(ip, now):
        try:
            ban = Ban.objects.get(ip=ip)
            if ban.expires_at > now:
                raise Banned(ban)
        except Ban.DoesNotExist:
            pass


class Tag(models.Model):
    name = models.CharField(max_length=10, primary_key=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return django.urls.reverse('core:by_tag', args=(self.name,))

    def get_threads(self):
        return self.threads_with_tag


def _get_image_path(post, filename):
    _, ext = os.path.splitext(filename)
    return str(uuid.uuid1()) + ext


class Post(models.Model):
    number = models.AutoField(primary_key=True)
    text = models.TextField(blank=True)
    image = models.ImageField(upload_to=_get_image_path,
                              height_field='image_width',
                              width_field='image_height',
                              null=True)
    image_height = models.IntegerField(null=True)
    image_width = models.IntegerField(null=True)
    timestamp = models.DateTimeField()
    bumptime = models.DateTimeField(null=True)
    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)
    tags = models.ManyToManyField('Tag', blank=True, related_name='threads_with_tag')
    ip = models.GenericIPAddressField(db_index=True)
    password_sha256 = models.CharField(max_length=64, null=True)

    def __str__(self):
        return f'#{self.number}: {self.text[:50]}'

    def get_absolute_url(self):
        if self.parent is not None:
            thread_url = django.urls.reverse('core:thread', args=(self.parent.number,))
            return f'{thread_url}#i{self.number}'
        else:
            return django.urls.reverse('core:thread', args=(self.number,))

    def get_anchored_url(self):
        thread_number = self.parent.number if self.parent is not None else self.number
        thread_url = django.urls.reverse('core:thread', args=(thread_number,))
        return f'{thread_url}#i{self.number}'

    @staticmethod
    def get_all_threads():
        return Post.objects.filter(parent=None)

    @staticmethod
    def create_thread(text, image, password, tags, ip):
        if not image:
            raise ContentMissing('image is required when creating a thread')
        now = timezone.now()
        Ban.check_ip(ip, now)
        thread = Post(
            text=_process_markup(text),
            image=image,
            timestamp=now,
            bumptime=now,
            parent=None,
            ip=ip,
            password_sha256=_get_password_sha256(password))
        thread.save()
        thread.tags.set(tags)
        return thread

    def get_comments(self):
        return Post.objects.filter(parent=self)

    def bumpable(self):
        return self.get_comments().count() < config.BUMP_LIMIT

    def add_comment(self, text, image, password, sage, ip):
        if not text and not image:
            raise ContentMissing('either a text or an image is required')
        now = timezone.now()
        Ban.check_ip(ip, now)
        comment = Post(
            text=_process_markup(text),
            image=image,
            timestamp=now,
            bumptime=None if sage else now,
            parent=self,
            ip=ip,
            password_sha256=_get_password_sha256(password))

        if not sage and self.bumpable():
            self.bumptime = now
            self.save()

        comment.save()
        return comment

    def delete_with_password(self, password):
        if self.password_sha256 is None:
            raise WrongPassword()
        if _get_password_sha256(password) != self.password_sha256:
            raise WrongPassword()
        self.delete()


def with_comment_counts(threads):
    """
    Yields `(thread, n_comments)` tuples.
    """
    query = Post.objects.filter(parent__in=threads)
    query = query.values('parent')
    query = query.annotate(ncomments=models.Count('*'))

    ncomments_by_number = {row['parent']: row['ncomments'] for row in query}

    for thread in threads:
        yield thread, ncomments_by_number.get(thread.number, 0)


def with_comment_previews(threads, nlast):
    """
    Yields `(thread, n_omitted_comments, preview_comments)` tuples.
    """
    for thread, ncomments in with_comment_counts(threads):
        preview = list(reversed(Post.objects.filter(parent=thread).order_by('-number')[:nlast]))
        yield thread, max(0, ncomments - len(preview)), preview


def _process_markup(text):
    nreplies = 0

    MAX_POSTNUM = (1 << 31) - 1

    def make_reply(postnum, content):
        nonlocal nreplies
        if nreplies >= config.MAX_POST_REPLIES:
            return content
        if postnum > MAX_POSTNUM:
            return content
        try:
            post = Post.objects.get(number=postnum)
        except Post.DoesNotExist:
            return content
        nreplies += 1
        return f'<a href="{post.get_anchored_url()}" class="toto-link" data-to="{postnum}">{content}</a>'

    return markup.process(text, make_reply)


def _get_password_sha256(password):
    if not password:
        return None
    return hashlib.sha256(password.encode()).hexdigest()
