'use strict';

function get_cookie(name) {
    let matches = document.cookie.match(new RegExp(
        '(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function get_abs_pos_rect(elem) {
    let body_rect = document.body.getBoundingClientRect();
    let elem_rect = elem.getBoundingClientRect();
    return {
        left:   elem_rect.left   - body_rect.left,
        right:  elem_rect.right  - body_rect.left,
        top:    elem_rect.top    - body_rect.top,
        bottom: elem_rect.bottom - body_rect.top,
    };
}

function insert_text(input, text) {
    input.focus();
    // Chrome, Opera, Safari, Edge
    if (document.execCommand('insertText', false, text)) {
        // OK
        return;
    }
    // Firefox
    if (typeof input.setRangeText === 'function') {
        let start = input.selectionStart;
        input.setRangeText(text);
        // update cursor to be at the end of insertion
        input.selectionStart = input.selectionEnd = start + text.length;
        // notify any possible listeners of the change
        let e = document.createEvent('UIEvent');
        e.initEvent('input', true, false);
        input.dispatchEvent(e);
        // OK
        return;
    }
    // the stupid way...
    let value = input.value;
    let start = input.selectionStart;
    let end = input.selectionEnd;
    input.value = value.slice(0, start) + text + value.slice(end);
    input.selectionStart = input.selectionEnd = start + text.length;
}

function find_posts_by_ip(ip) {
    let posts = document.getElementsByClassName('post');
    let result = [];
    for (let i = 0; i < posts.length; ++i) {
        let post = posts[i];
        let post_ip = post.getElementsByClassName('post-time')[0].getAttribute('data-ip');
        if (post_ip === ip) {
            result.push(post);
        }
    }
    return result;
}

function find_post_by_number(number) {
    return document.getElementById(`i${number}`);
}

function inject_csrf_token(xhr, method) {
    if (method != 'GET' && method != 'HEAD' && method != 'OPTIONS') {
        xhr.setRequestHeader('X-CSRFToken', get_cookie('csrftoken'));
    }
}

class Modal {
    constructor() {
        let modal = document.createElement('div');
        modal.className = 'modal';
        let content = document.createElement('div');
        content.className = 'modal-content';
        modal.appendChild(content);

        this.modal = modal;
        this.content = content;
    }

    show() {
        document.body.appendChild(this.modal);
    }

    close() {
        this.modal.remove();
    }
}

class DeletePostDialog {
    constructor() {
        let modal = new Modal();
        let form = document.createElement('form');
        form.innerHTML = `
            Password:
            <input type="password" autofocus required id="del-dialog-pw"/>
            <br/>`;

        let submit = document.createElement('input');
        submit.setAttribute('type', 'submit');
        submit.setAttribute('value', 'Delete');
        submit.onclick = () => {
            let pw = document.getElementById('del-dialog-pw').value;
            this.close();
            this.callback(pw);
            return false;
        };

        let cancel = document.createElement('input');
        cancel.setAttribute('type', 'button');
        cancel.setAttribute('value', 'Cancel');
        cancel.onclick = () => {
            modal.close();
        };

        form.appendChild(submit);
        form.appendChild(cancel);
        modal.content.appendChild(form);

        this.modal = modal;
        this.callback = null;
    }

    on_submit(callback) {
        this.callback = callback;
        return this;
    }

    show() {
        this.modal.show();
    }

    close() {
        this.modal.close();
    }
}

class Popup {
    constructor(x, y) {
        let content = document.createElement('div');
        content.className = 'popup';
        content.style.left = `${x}px`;
        content.style.top  = `${y}px`;

        this.content = content;
    }

    show() {
        document.body.appendChild(this.content);
    }

    close() {
        this.content.remove();
    }
}

class PostActionPopup {
    constructor() {
        this.popup = null;
        this.opened_for = null;
    }

    open_for(obj, x, y) {
        if (this.popup !== null) {
            this.close();
        }
        this.popup = new Popup(x, y);
        this.popup.show();
        this.opened_for = obj;
    }

    close() {
        this.popup.close();
        this.popup = null;
        this.opened_for = null;
    }

    toggle_for(obj, x, y) {
        if (this.opened_for === obj) {
            this.close();
            return false;
        }
        this.open_for(obj, x, y);
        return true;
    }

    append(elem) {
        this.popup.content.appendChild(elem);
        return this;
    }

    append_action(content, handler) {
        let a = document.createElement('a');
        a.className = 'post-action';
        if (typeof handler === 'string') {
            a.setAttribute('href', handler);
        } else {
            a.setAttribute('href', 'javascript:void(0)');
            a.onclick = () => {
                this.close();
                handler();
            }
        }
        a.innerHTML = content;

        let div = document.createElement('div');
        div.appendChild(a);
        this.popup.content.appendChild(div);
        return this;
    }
}

function insert_reply(post_num) {
    insert_text(document.getElementById('id_text'), `>>${post_num}`);
}

class Request {
    static _is_code_200(val) {
        return val == 200;
    }

    _forward(code, response) {
        if (this.is_code_ok(code)) {
            this.ok_callback(response);
        } else {
            this.error_callback(code, response);
        }
    }

    constructor(method, url, is_code_ok) {
        let xhr = new XMLHttpRequest();
        xhr.open(method, url, true);
        inject_csrf_token(xhr, method);

        xhr.onreadystatechange = () => {
            if (xhr.readyState != XMLHttpRequest.DONE) {
                return;
            }
            this._forward(xhr.status, xhr.responseText);
        };

        this.xhr = xhr;
        this.ok_callback = null;
        this.error_callback = null;
        this.body = null;
        this.is_code_ok = is_code_ok || Request._is_code_200;
    }

    then(callback) {
        this.ok_callback = callback;
        return this;
    }

    on_error(callback) {
        this.error_callback = callback;
        return this;
    }

    with_body(content_type, body) {
        this.xhr.setRequestHeader('Content-type', content_type);
        this.body = body;
        return this;
    }

    send() {
        this.xhr.send(this.body);
    }
}

class ApiRequest extends Request {
    _forward(code, response) {
        if (this.is_code_ok(code)) {
            let data = JSON.parse(response);
            if (data.ok) {
                this.ok_callback(data);
            } else {
                this.error_callback(data.error);
            }
        } else {
            this.error_callback(`Status ${code}`, response);
        }
    }
}

function traverse_toto_links(elem) {
    let toto_links = elem.getElementsByClassName('toto-link');
    for (let i = 0; i < toto_links.length; ++i) {
        let link = toto_links[i];
        link.onmouseover = toto_link_mouse_over;
    }
}

function toto_setup_timers(remove_func) {
    let tid = null;
    return {
        on_in: () => {
            if (tid == null) {
                return;
            }
            clearTimeout(tid);
            tid = null;
        },
        on_out: () => {
            if (tid !== null) {
                return;
            }
            tid = setTimeout(remove_func, 500);
        },
    };
}

let toto_cache = {};
function toto_link_mouse_over() {
    let postnum = this.getAttribute('data-to');

    let rect = get_abs_pos_rect(this);

    let post = document.createElement('article');
    post.className = 'post comment flying';
    let timers = toto_setup_timers(() => { post.remove(); });
    post.onmouseover = timers.on_in;
    post.onmouseout = timers.on_out;

    this.onmouseout = timers.on_out;

    let sel = (pos, whole, dim) => {
        return (pos < whole / 2) ? pos : pos - dim;
    };

    let show_post = (html) => {
        post.innerHTML = html;
        document.body.appendChild(post);

        let post_rect = get_abs_pos_rect(post);
        let x = sel(rect.left, window.innerWidth,  post_rect.right  - post_rect.left);
        let y = sel(rect.top,  window.innerHeight, post_rect.bottom - post_rect.top);
        post.style.left = `${x}px`;
        post.style.top  = `${y}px`;
        traverse_toto_links(post);
        traverse_post_actions(post);
    };

    let cached = toto_cache[postnum];
    if (cached !== undefined) {
        show_post(cached);
        return;
    }

    new Request(
        'GET',
        `${CFG_PATHS.api_render_post}?postnum=${postnum}`
    ).then((content) => {
        toto_cache[postnum] = content;
        show_post(content);
    }).on_error((code) => {
        show_post(`<span class="light-msg">Cannot fetch post: code ${code}</span>`);
    }).send();
}

function highlight_post(post) {
    post.className += ' ip-highlight';
}

function mark_post_deleted(post) {
    post.innerHTML = '<span class="light-msg">(Deleted)</span>';
}

function mark_post_banned(post) {
    post.className += ' banned';
}

function show_modal_working() {
    let modal = new Modal();
    modal.content.innerHTML = 'Working…';
    modal.show();
    return modal;
}

function show_modal_error(msg) {
    let modal = new Modal();
    modal.content.innerHTML = `<b>Error</b>: ${msg}.<br/>`;
    let close = document.createElement('input');
    close.setAttribute('type', 'button');
    close.setAttribute('value', 'Close');
    close.onclick = () => {
        modal.close();
    };
    modal.content.appendChild(close);
    modal.show();
}

function mod_del_request(params) {
    return new ApiRequest(
        'POST',
        CFG_PATHS.api_mod_del_post
    ).with_body(
        'application/x-www-form-urlencoded',
        `postnum=${params.postnum}&all_with_same_ip=${params.dell_all | 0}`
    );
}

function mod_ban_request(params) {
    let ip = encodeURIComponent(params.ip);
    let reason = encodeURIComponent(params.reason);
    let duration = encodeURIComponent(params.duration.toString());
    return new ApiRequest(
        'POST',
        CFG_PATHS.api_ban_user
    ).with_body(
        'application/x-www-form-urlencoded',
        `ip=${ip}&reason=${reason}&duration=${duration}`
    );
}

class BanDialog {
    constructor() {
        let modal = new Modal();
        let form = document.createElement('form');
        form.innerHTML = `
            <table>
                <tr><td>Reason:</td><td><input id="ban-reason" type="text" autofocus/></td></tr>
                <tr><td>Duration:</td><td><input id="ban-dur" type="number" required value="10"/>
                    <select id="ban-dur-units" required>
                        <option value="h">hours</option>
                        <option value="d" selected>days</option>
                        <option value="m">months</option>
                    </select>
                </td></tr>
            </table>`;
        let submit = document.createElement('input');
        submit.setAttribute('type', 'submit');
        submit.setAttribute('value', 'Ban');
        submit.onclick = () => {
            const units_table = {
                h: 60 * 60,
                d: 60 * 60 * 24,
                m: 60 * 60 * 24 * 30,
            };
            let reason = document.getElementById('ban-reason').value;
            let dur_val = document.getElementById('ban-dur').value;
            let dur_units = document.getElementById('ban-dur-units').value;
            let seconds = dur_val * units_table[dur_units];

            this.close();
            this.callback({reason: reason, duration: seconds});
            return false;
        };

        form.appendChild(submit);

        let cancel = document.createElement('input');
        cancel.setAttribute('type', 'button');
        cancel.setAttribute('value', 'Cancel');
        cancel.onclick = () => {
            this.close();
        };

        form.appendChild(cancel);

        modal.content.appendChild(form);

        this.modal = modal;
    }

    on_submit(callback) {
        this.callback = callback;
        return this;
    }

    show() {
        this.modal.show();
    }

    close() {
        this.modal.close();
    }
}

let pap = new PostActionPopup();

function post_actions_onclick() {
    let rect = get_abs_pos_rect(this);
    if (!pap.toggle_for(this, rect.left, rect.bottom)) {
        return;
    }
    let postnum = this.getAttribute('data-number');

    pap.append_action('Link', this.getAttribute('data-link'));

    pap.append_action('Delete', () => {
        new DeletePostDialog().on_submit((pw) => {
            let modal = show_modal_working();
            let encoded_pw = encodeURIComponent(pw);
            new ApiRequest(
                'POST',
                CFG_PATHS.api_del_post
            ).with_body(
                'application/x-www-form-urlencoded',
                `postnum=${postnum}&password=${encoded_pw}`
            ).then(() => {
                modal.close();
                mark_post_deleted(find_post_by_number(postnum));
            }).on_error((msg) => {
                modal.close();
                show_modal_error(msg);
            }).send();
        }).show();
    });

    pap.append_action('Close', () => {
        // it closes automatically
    });

    if (!CFG_IS_MOD) {
        return;
    }

    let ip = this.getAttribute('data-ip');

    let preface = document.createElement('div');
    preface.innerHTML = `
        <div class="popup-section">★ Mod actions ★</div>
        <b>IP</b>: ${ip}`;
    pap.append(preface);

    pap.append_action('Highlight posts', () => {
        find_posts_by_ip(ip).forEach(highlight_post);
    });

    pap.append_action('Find posts', `${CFG_PATHS.find_posts_by_ip}?ip=${encodeURIComponent(ip)}`);

    pap.append_action('Delete', () => {
        let modal = show_modal_working();
        mod_del_request({
            postnum: postnum,
            del_all: false,
        }).then(() => {
            modal.close();
            let post = find_post_by_number(postnum);
            mark_post_deleted(post);
        }).on_error((msg) => {
            modal.close();
            show_modal_error(msg);
        }).send();
    });

    pap.append_action('Delete all', () => {
        let modal = show_modal_working();
        mod_del_request({
            postnum: postnum,
            del_all: true,
        }).then(() => {
            modal.close();
            find_posts_by_ip(ip).forEach(mark_post_deleted);
        }).on_error((msg) => {
            modal.close();
            show_modal_error(msg);
        }).send();
    });

    pap.append_action('Ban', () => {
        new BanDialog().on_submit((params) => {
            let modal = show_modal_working();
            mod_ban_request({
                ip: ip,
                reason: params.reason,
                duration: params.duration,
            }).then(() => {
                modal.close();
                let post = find_post_by_number(postnum);
                mark_post_banned(post);
            }).on_error((msg) => {
                modal.close();
                show_modal_error(msg);
            }).send();
        }).show();
    });

    pap.append_action('Ban &amp; Delete', () => {
        new BanDialog().on_submit((params) => {
            let modal = show_modal_working();
            mod_ban_request({
                ip: ip,
                reason: params.reason,
                duration: params.duration,
            }).then(() => {

                mod_del_request({
                    postnum: postnum,
                    del_all: false,
                }).then(() => {
                    modal.close();
                    let post = find_post_by_number(postnum);
                    mark_post_deleted(post);
                    mark_post_banned(post);
                }).on_error((msg) => {
                    modal.close();
                    show_modal_error(msg);
                }).send();

            }).on_error((msg) => {
                modal.close();
                show_modal_error(msg);
            }).send();
        }).show();
    });

    pap.append_action('Ban &amp; Delete all', () => {
        new BanDialog().on_submit((params) => {
            let modal = show_modal_working();
            mod_ban_request({
                ip: ip,
                reason: params.reason,
                duration: params.duration,
            }).then(() => {

                mod_del_request({
                    postnum: postnum,
                    del_all: true,
                }).then(() => {
                    modal.close();
                    find_posts_by_ip(ip).forEach((post) => {
                        mark_post_deleted(post);
                        mark_post_banned(post);
                    });
                }).on_error((msg) => {
                    modal.close();
                    show_modal_error(msg);
                }).send();

            }).on_error((msg) => {
                modal.close();
                show_modal_error(msg);
            }).send();
        }).show();
    });
}

function traverse_post_actions(elem) {
    let action_links = elem.getElementsByClassName('post-time');
    for (let i = 0; i < action_links.length; ++i) {
        let link = action_links[i];
        link.onclick = post_actions_onclick;
    }
}

document.addEventListener('DOMContentLoaded', () => {
    traverse_toto_links(document);
    traverse_post_actions(document);
});
