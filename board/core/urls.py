from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from .views import board, api



app_name = 'core'


urlpatterns = [
    path('',                                     board.board, name='all'),
    path('page/<int:page>',                      board.board, name='all'),
    path('tag/<slug:tag_name>/',                 board.board, name='by_tag'),
    path('tag/<slug:tag_name>/page/<int:page>',  board.board, name='by_tag'),

    path('catalog',                                     board.catalog, name='catalog_all'),
    path('catalog/<int:page>',                          board.catalog, name='catalog_all'),
    path('catalog/tag/<slug:tag_name>/',                board.catalog, name='catalog_tag'),
    path('catalog/tag/<slug:tag_name>/page/<int:page>', board.catalog, name='catalog_tag'),

    path('thread/<int:number>', board.thread, name='thread'),

    path('find_posts_by_ip', board.find_posts_by_ip, name='find_posts_by_ip'),
    path('api/mod_del_post', api.mod_del_post,       name='api_mod_del_post'),
    path('api/ban_user',     api.ban_user,           name='api_ban_user'),
    path('api/render_post',  api.render_post,        name='api_render_post'),
    path('api/del_post',     api.del_post,           name='api_del_post'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
