from django.contrib import admin
import core.models


admin.site.register(core.models.Ban)
admin.site.register(core.models.Tag)
admin.site.register(core.models.Post)
