from . import models


def user_processor(request):
    return {
        'is_mod': request.user.has_perm('core.change_post'),
    }


def existing_tags_processor(request):
    return {
        'existing_tags': models.Tag.objects.order_by('name'),
    }
