import datetime
from django.shortcuts import render
from django.http import (
    Http404,
    HttpResponseBadRequest,
    HttpResponseNotAllowed,
    JsonResponse,
)
from django.db.models import prefetch_related_objects
from .utils import mod_only
from .. import models
from .. import config
from ..abstracts.zero_based_paginator import ZeroBasedPaginator


@mod_only
def mod_del_post(request):
    if request.method == 'POST':
        try:
            postnum = int(request.POST['postnum'])
            all_with_same_ip = bool(int(request.POST['all_with_same_ip']))
        except (KeyError, ValueError):
            return HttpResponseBadRequest()

        try:
            post = models.Post.objects.get(number=postnum)
        except models.Post.DoesNotExist:
            return JsonResponse({'ok': False, 'error': 'post not found'})

        if all_with_same_ip:
            models.Post.objects.filter(ip=post.ip).delete()
        else:
            post.delete()

        return JsonResponse({'ok': True})

    return HttpResponseNotAllowed(['POST'])


@mod_only
def ban_user(request):
    if request.method == 'POST':
        try:
            ip = request.POST['ip']
            reason = request.POST['reason']
            duration = int(request.POST['duration'])
        except (KeyError, ValueError):
            return HttpResponseBadRequest()

        ban = models.Ban.create_with_duration(
            ip=ip,
            reason=reason,
            duration=datetime.timedelta(seconds=duration))
        ban.save()

        return JsonResponse({'ok': True})

    return HttpResponseNotAllowed(['POST'])


def render_post(request):
    if request.method == 'GET':
        try:
            postnum = int(request.GET['postnum'])
        except (KeyError, ValueError):
            return HttpResponseBadRequest()

        try:
            post = models.Post.objects.get(number=postnum)
        except models.Post.DoesNotExist:
            raise Http404()

        return render(request, 'post.html', {
            'post': post,
            'has_tags': False,
            'open_link': False,
        })

    return HttpResponseNotAllowed(['GET'])


def del_post(request):
    if request.method == 'POST':
        try:
            postnum = int(request.POST['postnum'])
            password = request.POST['password']
        except (KeyError, ValueError):
            return HttpResponseBadRequest()

        try:
            post = models.Post.objects.get(number=postnum)
        except models.Post.DoesNotExist:
            raise Http404()

        try:
            post.delete_with_password(password)
        except models.WrongPassword:
            return JsonResponse({'ok': False, 'error': 'wrong password'})
        else:
            return JsonResponse({'ok': True})

    return HttpResponseNotAllowed(['POST'])
