from django.shortcuts import render
from django.http import (
    Http404,
    HttpResponseRedirect,
    HttpResponseBadRequest,
)
from django.db.models import prefetch_related_objects
from .utils import get_ip, mod_only
from .. import forms
from .. import models
from .. import config
from ..abstracts.zero_based_paginator import ZeroBasedPaginator


def board(request, tag_name=None, page_index=0):
    if request.method == 'POST':
        form = forms.ThreadForm(request.POST, request.FILES)

        if form.is_valid():
            try:
                thread = models.Post.create_thread(
                    text=form.cleaned_data['text'],
                    image=form.cleaned_data['image'],
                    password=form.cleaned_data['password'],
                    tags=form.cleaned_data['tags'],
                    ip=get_ip(request))
            except models.Banned as ex:
                return render(request, 'banned.html', {
                    'ban': ex.ban,
                })
            return HttpResponseRedirect(thread.get_absolute_url())
    else:
        form = forms.ThreadForm()

    if tag_name is not None:
        try:
            threads = models.Tag.objects.get(name=tag_name).get_threads()
        except models.Tag.DoesNotExist:
            raise Http404()
    else:
        threads = models.Post.get_all_threads()

    threads = threads.order_by('-bumptime')
    page = ZeroBasedPaginator(threads, config.THREADS_PER_PAGE).page(page_index)

    prefetch_related_objects(page.object_list, 'tags')

    return render(request, 'board.html', {
        'tag_name': tag_name,
        'data':     models.with_comment_previews(page.object_list, config.PREVIEW_COMMENTS),
        'page':     page,
        'form':     form,
    })


def catalog(request, tag_name=None, page_index=0):
    if request.method == 'GET':
        if tag_name is not None:
            try:
                threads = models.Tag.objects.get(name=tag_name).get_threads()
            except models.Tag.DoesNotExist:
                raise Http404()
        else:
            threads = models.Post.get_all_threads()

        threads = threads.order_by('-bumptime')

        page = ZeroBasedPaginator(threads, config.CATALOG_SIZE).page(page_index)

        prefetch_related_objects(page.object_list, 'tags')

        return render(request, 'catalog.html', {
            'tag_name': tag_name,
            'data':     models.with_comment_counts(page.object_list),
            'page':     page,
        })

    return HttpResponseNotAllowed(['GET'])


def thread(request, number):
    try:
        thread = models.Post.objects.get(number=number)
    except models.Post.DoesNotExist:
        raise Http404()

    if thread.parent is not None:
        raise Http404()

    if request.method == 'POST':
        form = forms.CommentForm(request.POST, request.FILES)

        if form.is_valid():
            try:
                comment = thread.add_comment(
                    text=form.cleaned_data['text'],
                    image=form.cleaned_data['image'],
                    password=form.cleaned_data['password'],
                    sage=form.cleaned_data['sage'],
                    ip=get_ip(request))
            except models.Banned as ex:
                return render(request, 'banned.html', {
                    'ban': ex.ban,
                })
            return HttpResponseRedirect(comment.get_absolute_url())
    else:
        form = forms.CommentForm()

    return render(request, 'thread.html', {
        'thread':   thread,
        'comments': thread.get_comments().order_by('number'),
        'form':     form,
    })


@mod_only
def find_posts_by_ip(request):
    if request.method == 'GET':
        try:
            ip = request.GET['ip']
            page_index = int(request.GET.get('page', '0'))
        except (KeyError, ValueError):
            return HttpResponseBadRequest()
        if page_index < 0:
            return HttpResponseBadRequest()

        page = ZeroBasedPaginator(
                models.Post.objects.filter(ip=ip).order_by('number'),
                config.POSTS_BY_IP_PER_PAGE
            ).page(page_index)

        prefetch_related_objects(page.object_list, 'tags')

        return render(request, 'find-posts-by-ip.html', {
            'ip':   ip,
            'page': page,
        })

    return HttpResponseNotAllowed(['GET'])
