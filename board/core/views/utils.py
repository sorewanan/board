from django.http import HttpResponseForbidden


def mod_only(func):
    def wrapper(request, *args, **kwargs):
        if not request.user.has_perm('core.change_post'):
            return HttpResponseForbidden()
        return func(request, *args, **kwargs)
    return wrapper


def get_ip(request):
    return request.META.get('REMOTE_ADDR')
