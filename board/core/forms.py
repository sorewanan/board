from django import forms
from . import models


_MAX_PASS_LENGTH = 32


class ThreadForm(forms.Form):
    text = forms.CharField(widget=forms.Textarea, required=False)
    password = forms.CharField(max_length=_MAX_PASS_LENGTH,
                               widget=forms.PasswordInput(),
                               required=False)
    tags = forms.ModelMultipleChoiceField(
        queryset=models.Tag.objects.order_by('name'),
        required=False)
    image = forms.ImageField(required=False)


class CommentForm(forms.Form):
    text = forms.CharField(widget=forms.Textarea, required=False)
    sage = forms.BooleanField(required=False)
    password = forms.CharField(max_length=_MAX_PASS_LENGTH,
                               widget=forms.PasswordInput(),
                               required=False)
    image = forms.ImageField(required=False)
