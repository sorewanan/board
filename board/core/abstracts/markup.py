import re
import html


def _escape(text):
    return html.escape(text).replace('\n', '<br/>')


class _MarkupElement:
    def __init__(self, pattern):
        self.pattern = pattern

    def __call__(self, match):
        raise NotImplementedError()


def _process_elem(text, element):
    prev = 0
    for match in element.pattern.finditer(text):
        yield False, text[prev:match.start()]
        yield from element(match)
        prev = match.end()
    yield False, text[prev:]


def _process_all(text, elements):
    if elements:
        return ''.join(
            chunk if is_fixed else _process_all(chunk, elements[1:])
            for is_fixed, chunk in _process_elem(text, elements[0])
        )
    else:
        return _escape(text)


class _ReplyElement(_MarkupElement):
    def __init__(self, make_link):
        super().__init__(re.compile(r'>>([0-9]+)'))
        self.make_link = make_link

    def __call__(self, match):
        yield True, self.make_link(int(match[1]), _escape(match[0]))


class _QuoteElement(_MarkupElement):
    def __init__(self):
        super().__init__(re.compile(r'^>(?!>[0-9]).*$', re.MULTILINE))

    def __call__(self, match):
        yield True, '<blockquote>'
        yield False, match[0]
        yield True, '</blockquote>'


class _BoldElement(_MarkupElement):
    def __init__(self):
        super().__init__(re.compile(r'\*\*\b(.+?)\b\*\*'))

    def __call__(self, match):
        yield True, '<b>'
        yield False, match[1]
        yield True, '</b>'


class _ItalicElement(_MarkupElement):
    def __init__(self):
        super().__init__(re.compile(r'\*\b(.+?)\b\*'))

    def __call__(self, match):
        yield True, '<i>'
        yield False, match[1]
        yield True, '</i>'


class _InlineCodeElement(_MarkupElement):
    def __init__(self):
        super().__init__(re.compile(r'`\b(.+?)\b`'))

    def __call__(self, match):
        yield True, '<code>'
        yield True, _escape(match[1])
        yield True, '</code>'


class _BlockCodeElement(_MarkupElement):
    def __init__(self):
        super().__init__(re.compile(r'````(.+?)````', re.DOTALL))

    def __call__(self, match):
        yield True, '<pre>'
        yield True, _escape(match[1])
        yield True, '</pre>'


class _StrikethroughElement(_MarkupElement):
    def __init__(self):
        super().__init__(re.compile(r'~~\b(.+?)\b~~'))

    def __call__(self, match):
        yield True, '<s>'
        yield False, match[1]
        yield True, '</s>'


class _SpoilerElement(_MarkupElement):
    def __init__(self):
        super().__init__(re.compile(r'%%\b(.+?)\b%%'))

    def __call__(self, match):
        yield True, '<span class="spoiler">'
        yield False, match[1]
        yield True, '</span>'


def process(text, make_link):
    """
    Converts a text in our markup format to HTML.

    `make_link` is a callable that is called with two arguments: the post number (int) and the
    HTML-escaped content (str). It should return a str with HTML markup.

    >>> process('>a >>1 b >>2 c', lambda num, content: f'<a href="#i{num}">{content}</a>')
    <blockquote>a <a href="#i1">&gt;&gt;1</a> b <a href="#i2">&gt;&gt;2</a> c</blockquote>
    """
    return _process_all(text, (
        _QuoteElement(),
        _SpoilerElement(),
        _BoldElement(),
        _ItalicElement(),
        _InlineCodeElement(),
        _BlockCodeElement(),
        _StrikethroughElement(),
        _ReplyElement(make_link),
    ))
