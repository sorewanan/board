def _div_ceil(a, b):
    return (a + b - 1) // b


class InvalidPage(BaseException):
    pass


def _validate_page_number(number):
    if not isinstance(number, int):
        raise InvalidPage('page number is not integer')
    if number < 0:
        raise InvalidPage('page number is negative')


class Page:
    def __init__(self, object_list, number, paginator):
        self.object_list = list(object_list)
        self.number = number
        self.paginator = paginator

    def _is_out_of_bounds(self):
        return self.number >= self.paginator.num_pages

    def has_next(self):
        return self.number + 1 < self.paginator.num_pages

    def has_previous(self):
        return self.number > 0

    def has_other_pages(self):
        return self.has_next() or self.has_previous()

    def next_page_number(self):
        if not self.has_next():
            raise InvalidPage('no next page')
        return self.number + 1

    def previous_page_number(self):
        if not self.has_previous(self):
            raise InvalidPage('no previous page')
        return self.number - 1

    def start_index(self):
        return 1 + self.paginator.per_page * self.number

    def end_index(self):
        return self.paginator.per_page * self.number + len(self.object_list)


class ZeroBasedPaginator:
    def __init__(self, query_set, per_page):
        self.query_set = query_set
        self.per_page = per_page
        self.count = query_set.count()
        self.num_pages = _div_ceil(self.count, per_page)
        self.page_range = range(self.num_pages)

    def page(self, number):
        _validate_page_number(number)
        object_list = self.query_set[self.per_page * number:]
        object_list = object_list[:self.per_page]
        return Page(object_list=object_list, number=number, paginator=self)
